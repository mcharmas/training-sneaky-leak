package pl.charmas.training.sneakyleak;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;

import javax.inject.Inject;

public class MainActivity extends AppCompatActivity {

    @Inject
    Picasso picasso;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        App.getComponent(this).inject(this);
        setContentView(R.layout.activity_main);

        RecyclerView recyclerView = findViewById(R.id.content);
        recyclerView.setLayoutManager(new GridLayoutManager(this, 2));
        recyclerView.setAdapter(new ImagesAdapter(picasso, this, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, MainActivity.class));
            }
        }));
    }

    private static class ImagesAdapter extends RecyclerView.Adapter<ImagesAdapter.ImageViewHolder> {
        private final Picasso picasso;
        private final LayoutInflater inflater;
        private final View.OnClickListener onClickListener;

        private ImagesAdapter(Picasso picasso, Context context, View.OnClickListener onClickListener) {
            this.picasso = picasso;
            this.inflater = LayoutInflater.from(context);
            this.onClickListener = onClickListener;
        }

        @NonNull
        @Override
        public ImageViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            return new ImageViewHolder(inflater.inflate(R.layout.list_item_image, parent, false), onClickListener);
        }

        @Override
        public void onBindViewHolder(ImageViewHolder holder, int position) {
            holder.loadRandomImage(picasso, position);
        }

        @Override
        public int getItemCount() {
            return 200;
        }

        private static class ImageViewHolder extends RecyclerView.ViewHolder {
            ImageViewHolder(View itemView, View.OnClickListener onClickListener) {
                super(itemView);
                itemView.setOnClickListener(onClickListener);
            }

            void loadRandomImage(Picasso picasso, int position) {
                picasso.load("https://unsplash.it/800?random&" + position)
                        .centerCrop()
                        .fit()
                        .into((ImageView) itemView);
            }
        }
    }
}

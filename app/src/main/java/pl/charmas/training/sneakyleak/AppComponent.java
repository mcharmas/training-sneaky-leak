package pl.charmas.training.sneakyleak;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = {AndroidModule.class})
public interface AppComponent {
    void inject(MainActivity target);
}
